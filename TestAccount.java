public class TestAccount {

    public static void main(String[] args) {
        
        Account myAccount = new Account();

        myAccount.setName("David");
        myAccount.setBalance(1000000.00);
        
        System.out.println("Name:" + myAccount.getName());
        System.out.println("Moneys: " + myAccount.getBalance());
        
        myAccount.addInterest();

        System.out.println("Name:" + myAccount.getName());
        System.out.println("Moneys: " + myAccount.getBalance());

        Account arrayOfAccounts[] = new Account[5];

        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};
        double[] amounts = {23,5444,2,345,34};
        
        for (int i = 0; i < arrayOfAccounts.length; i++) {
           
            arrayOfAccounts[i] = new Account();
            arrayOfAccounts[i].setName(names[i]);
            arrayOfAccounts[i].setBalance(amounts[i]);
            
            System.out.println(arrayOfAccounts[i].getName());
            System.out.println(arrayOfAccounts[i].getBalance());

            arrayOfAccounts[i].addInterest();
            System.out.println(arrayOfAccounts[i].getName());
            System.out.println(arrayOfAccounts[i].getBalance());
            
        }
    }
    
}
