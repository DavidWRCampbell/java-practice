public class TestAccount2 {

    public static void main(String[] args) {
        
        Account.setInterestRate(2.0);
        Account arrayOfAccounts[] = new Account[5];

        arrayOfAccounts[0] = new Account("Name 1", 10);
        arrayOfAccounts[1] = new Account("Name 2", 20);
        arrayOfAccounts[2] = new Account("Name 3", 30);
        arrayOfAccounts[3] = new Account("Name 4", 40);
        arrayOfAccounts[4] = new Account("Name 5", 50);

        // for (int i = 0; i < arrayOfAccounts.length; i++) {

        //     System.out.println(arrayOfAccounts[i].getName());
        //     System.out.println(arrayOfAccounts[i].getBalance());

        //     arrayOfAccounts[i].addInterest();

        //     System.out.println(arrayOfAccounts[i].getName());
        //     System.out.println(arrayOfAccounts[i].getBalance());
            
        // }

        arrayOfAccounts[0].withdraw();
        arrayOfAccounts[1].withdraw();
        arrayOfAccounts[2].withdraw(40);
        arrayOfAccounts[3].withdraw(40);


    }
    
}
