public class Account {

    private double balance;
    private String name;
    private static Double interestRate;

    

    public Account(String name, double balance) {
        this.name = name;
        this.balance = balance;
    }

    public Account() {
        this.balance = 50.00;
        this.name = "Your Name Here";
    }

    public static Double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(Double interestRate) {
        Account.interestRate = interestRate;
    }

    public double getBalance(){
        return this.balance;
    }

    public String getName(){
        return this.name;
    }

    public void setBalance(double balance){
        this.balance = balance;
    }

    public void setName(String name){
        this.name = name;
    }

    public void addInterest() {
        this.balance = this.balance * interestRate;
    }

    public boolean withdraw (double amount) {
        if (this.balance - amount >= 0) {
            this.balance -= amount;
            System.out.println(this.name + " withdrew " + amount 
            + ", balance is now: " + this.balance);
            return true;
        }
        System.out.println(this.name + " has insufficient funds");
        return false;
    }

    public boolean withdraw () {
        return withdraw(20);
    }


}

